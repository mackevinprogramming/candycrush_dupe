using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundTile : MonoBehaviour
{
    // Eventually come back and change all these "Public" items to [SerializedField] private
    public GameObject[] dots; 


    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Dont need to access this from anywhere else

    void Initialize()   // In Unity drag dot prefabs onto 'TileBackground' prefab Dots in the Script portion of it
    {
        // Once this is created we want to create a 'Dot' (game piece) in it

        // This is chosing a Random colored dot  between 1 and however many dots we have
        int dotToUse = Random.Range(0, dots.Length);

        // Creating a GameObj in the game named 'dot' and Instantiating it in the board
        GameObject dot = Instantiate(dots[dotToUse], transform.position, Quaternion.identity);

        // Then create a group and name it to clean up Hierarchy in Unity
        dot.transform.parent = this.transform; // transform makes it be a child of the background tile it spawned on
        dot.name = this.transform.name;
        // Below is another way to write the dot's name in Hierarchy
        // dot.name = this.gameObject.name;
    }
}
