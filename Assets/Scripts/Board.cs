using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public int width;
    public int height;
    public GameObject tilePrefab;
    // Is from the Script "BackgroundTile" which has an array of 1x1 named 'allTiles'
    private BackgroundTile[,] allTiles;


    // Start is called before the first frame update
    void Start()
    {
        allTiles= new BackgroundTile[height, width];
        // Call the Setup to start the Board
        SetUp();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void SetUp()
    {
        // i will represent the x-coordinate
        for (int i = 0; i < width; i++)
        {
            // j represents the y-coordinate
            for (int j = 0; j < height; j++)
            {
                // This is were we actually create the tiles
                Vector2 tempPosition = new Vector2(i,j);
                /* Changed it from just "Instantiating" which created a ton of individual blocks to 'backgroundTile'
                    which made it a variable that we can make adjustments to it easier and it creates only 1 item
                    in Unity's "Hierarchy" when the game starts running
                */
                GameObject backgroundTile = 
                    Instantiate(tilePrefab, tempPosition, Quaternion.identity) as GameObject;   // Writing 'as GameObject' lets Unity know this Variable is a GameObject
                
                // Setting the "parent" of this gameObject to the "BoardObject" so that will all go into 1 tidy group
                backgroundTile.transform.parent = this.transform;
                
                //Changes Names (in Unity's Hierarchy) of each individual cell by its "x" and  "y" location
                // which is its Vector Space from above 'i' and 'j'
                backgroundTile.name = "( " + i + ", " + j + " )";
            
            
            }
        }
    }
}
